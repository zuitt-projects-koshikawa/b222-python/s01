name = 'Tom'
age = 28
occupation = 'Software Engineer'
movie = 'spider-man'
rating = '4 out of 5'

print(f"I am {name}, and I'am {age}, I work as a {occupation}, and my rating for {movie} is {rating}")

num1, num2, num3 = 10, 20, 30

print(num1 *num2)
print(num1 < num3)
print(num3 + num2)